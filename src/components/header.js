import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import { DiLinux } from 'react-icons/di';

const Header = ({ siteTitle }) => (
  <nav className="nav navbar-nav">
    <div
      className="navbar"
      style={{
        margin: `0 auto`,
      }}
    >
      <h1 style={{ margin: 0, color: `white` }}>
        <Link
          to="/"
          style={{
            textDecoration: `none`,
            color: `white`,
            marginRight: `3rem`,
          }}
        >
          <DiLinux />
        </Link>
        {siteTitle}
      </h1>
    </div>
  </nav>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
