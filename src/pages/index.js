import React from 'react';
import { Link } from 'gatsby';
import { FaLinux } from 'react-icons/fa';
import Layout from '../components/layout';
import SEO from '../components/seo';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, 'wiki']} />
    <h1 className="text-center">Wiki Home</h1>
    <hr />
    <p>
      Add some new pages by following the instructions at the{' '}
      <Link to="/about">About</Link> page!
    </p>
  </Layout>
);

export default IndexPage;
