---
title: About
path: '/about'
---

## About This Wiki

This wiki is the culmination of an effort that arose out of a need to store the information generated from my explorations of the Linux world in a
format that is more akin to how this information is best absorbed while wishing to keep the notes themselves backed up
using Git.

## Creating wiki posts

1. Create md file in `wiki/<topic>/<post>.md`

2. Setup frontmatter at the top of the file like so:

```
---
title: 'blah'
path: '/<topic>/<post>.md'
---
```

3. Add post content 👌
