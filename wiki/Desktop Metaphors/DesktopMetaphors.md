---
title: 'Desktop Metaphors'
path: '/DesktopOptions/DesktopOptions'
---

**Desktop Metaphor** - a bundle of programs that run atop of the **operating system**
